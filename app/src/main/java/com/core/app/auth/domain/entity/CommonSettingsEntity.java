package com.core.app.auth.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "users_settings")
public class CommonSettingsEntity implements Serializable {

  @Id
  @JsonIgnore
  @Column(name = "id")
  @GeneratedValue
  private Long id;

  @Column(name = "user_id")
  private Long userId;

  @Column(name = "tg")
  private String tg;

  @Column(name = "phone")
  private String phone;

  @Column(name = "currency")
  private String currency;

  @Column(name = "timezone")
  private String timezone;

}
