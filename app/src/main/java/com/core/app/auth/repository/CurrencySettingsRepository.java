package com.core.app.auth.repository;

import com.core.app.auth.domain.entity.CurrencySettingsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencySettingsRepository extends JpaRepository<CurrencySettingsEntity, Long> {

}
