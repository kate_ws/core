package com.core.app.auth.controller;

import com.core.app.auth.token.SecuredRoles;
import com.core.app.domain.dto.response.RestResponseDto;
import com.core.app.auth.repository.UserRepository;

import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("protected")
public class TestMethodProtectedRestController {

  @Autowired
  private UserRepository userRepository;

  @RequestMapping(value = "admin", method = RequestMethod.GET)
  @Secured(SecuredRoles.ADMIN)
  public ResponseEntity<?> getProtectedGreeting() {
    return ResponseEntity.ok("ADMIN");
  }

  @RequestMapping(value = "user", method = RequestMethod.GET)
  @Secured(SecuredRoles.USER)
  public ResponseEntity<?> user() {
    return ResponseEntity.ok("ROLE_USER");
  }

  @RequestMapping(value = "developer", method = RequestMethod.GET)
  @Secured(SecuredRoles.DEVELOPER)
  public ResponseEntity<?> developer() {
    return ResponseEntity.ok("DEVELOPER");
  }

  @RequestMapping(value = "all", method = RequestMethod.GET)
  public ResponseEntity<RestResponseDto> all() {
    RestResponseDto respons = new RestResponseDto();
    respons.setSuccess(true);
    respons.setData(userRepository.findAll());
    respons.setTimestamp();

    return new ResponseEntity<>(respons, HttpStatus.OK);
  }

  @ModelAttribute
  public void setVaryResponseHeader(HttpServletResponse response) {
    response.setHeader("Vary", "Accept");
  }
}