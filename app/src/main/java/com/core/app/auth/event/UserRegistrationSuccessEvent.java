package com.core.app.auth.event;


import com.core.app.auth.domain.entity.UserEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.context.ApplicationEvent;

@Data
@EqualsAndHashCode(callSuper = false)
public class UserRegistrationSuccessEvent extends ApplicationEvent {

  private String recipient;

  private String verificationToken;

  protected UserEntity user;

  public UserRegistrationSuccessEvent(java.lang.Object source) {
    super(source);
  }

}
