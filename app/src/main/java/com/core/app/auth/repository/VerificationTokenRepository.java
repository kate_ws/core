package com.core.app.auth.repository;

import com.core.app.auth.domain.entity.UserEntity;
import com.core.app.auth.domain.entity.VerificationTokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VerificationTokenRepository extends JpaRepository<VerificationTokenEntity, Long> {

  Optional<VerificationTokenEntity> findByToken(String token);

  VerificationTokenEntity findByUser(UserEntity user);

}
