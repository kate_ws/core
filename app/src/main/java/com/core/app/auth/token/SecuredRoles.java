package com.core.app.auth.token;

public class SecuredRoles {

  public static final String ADMIN = "ROLE_ADMIN";
  public static final String USER = "ROLE_USER";
  public static final String DEVELOPER = "ROLE_DEVELOPER";
}
