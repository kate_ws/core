package com.core.app.auth.service.api;

import com.core.app.auth.domain.dto.JwtAuthenticationRequest;
import com.core.app.domain.dto.response.RestResponseDto;

import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestBody;

public interface AuthenticationService {

  RestResponseDto createToken(@RequestBody JwtAuthenticationRequest authenticationRequest);

  RestResponseDto refreshToken(HttpServletRequest request);
}
