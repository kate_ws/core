package com.core.app.auth.controller;

import static com.core.app.common.Constants.HEADER_SECURITY_TOKEN;

import com.core.app.auth.domain.dto.UserResetPasswordDto;
import com.core.app.auth.domain.dto.UserRestorePasswordDto;
import com.core.app.auth.domain.dto.UserRestorePasswordInitDto;
import com.core.app.auth.domain.dto.UserSignUpRequestDto;
import com.core.app.auth.token.SecuredRoles;
import com.core.app.domain.dto.response.RestResponseDto;
import com.core.app.auth.exceptions.WrongCurrentPasswordException;
import com.core.app.auth.service.api.UserService;
import io.swagger.annotations.ApiImplicitParam;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class UserController {

  @Autowired
  private UserService userService;

  @PostMapping(value = "sign-up")
  public ResponseEntity<RestResponseDto> signUp(
      @RequestBody UserSignUpRequestDto signUpRequestDto) {
    RestResponseDto response = userService.registerUser(signUpRequestDto);
    return ResponseEntity.ok(response);
  }

  @GetMapping(value = "sign-in/confirm")
  public ResponseEntity<RestResponseDto> confirmSignUp(@RequestParam("token") String requestToken) {
    RestResponseDto response = userService.confirmRegistration(requestToken);
    return ResponseEntity.ok(response);
  }

  @ApiImplicitParam(name = HEADER_SECURITY_TOKEN, value = "Header security token",
      required = true, dataType = "string", paramType = "header")
  @PostMapping(value = "password/reset")
  @Secured({SecuredRoles.USER, SecuredRoles.ADMIN, SecuredRoles.DEVELOPER})
  public ResponseEntity<RestResponseDto> resetPassword(
      @RequestBody UserResetPasswordDto resetPasswordDto) throws WrongCurrentPasswordException {
    RestResponseDto response = userService.resetPassword(resetPasswordDto);
    return ResponseEntity.ok(response);
  }

  @PostMapping(value = "password/restore/init")
  public ResponseEntity<RestResponseDto> restorePasswordInit(
      @RequestBody UserRestorePasswordInitDto request) {
    RestResponseDto response = userService.restorePassword(request);
    return ResponseEntity.ok(response);
  }

  @PostMapping(value = "password/restore/confirm")
  public ResponseEntity<RestResponseDto> restorePasswordConfirm(
      @RequestBody UserRestorePasswordDto request) {
    RestResponseDto response = userService.restorePasswordConfirm(request);
    return ResponseEntity.ok(response);
  }

  @GetMapping(value = "captcha/get")
  public ResponseEntity<RestResponseDto> getCaptcha(HttpServletRequest request) {
    RestResponseDto restResponse = userService.getCaptcha(request);
    return ResponseEntity.ok(restResponse);
  }

  @GetMapping(value = "captcha/validate")
  public ResponseEntity<RestResponseDto> validateCaptcha(HttpServletRequest request) {
    RestResponseDto restResponse = userService.validateCaptcha(request);
    return ResponseEntity.ok(restResponse);
  }


  @ExceptionHandler(WrongCurrentPasswordException.class)
  public ResponseEntity<RestResponseDto> wrongCurrentPassword(WrongCurrentPasswordException ex) {
    RestResponseDto errorResponse = new RestResponseDto();
    errorResponse.setSuccess(false);
    errorResponse.setMessage(ex.getMessage());

    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }

}
