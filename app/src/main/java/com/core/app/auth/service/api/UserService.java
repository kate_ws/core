package com.core.app.auth.service.api;

import com.core.app.auth.domain.dto.UserResetPasswordDto;
import com.core.app.auth.domain.dto.UserRestorePasswordDto;
import com.core.app.auth.domain.dto.UserRestorePasswordInitDto;
import com.core.app.auth.domain.dto.UserSignUpRequestDto;
import com.core.app.auth.domain.entity.UserEntity;
import com.core.app.auth.domain.entity.VerificationTokenEntity;
import com.core.app.common.service.api.GenericService;
import com.core.app.domain.dto.response.RestResponseDto;
import com.core.app.auth.exceptions.WrongCurrentPasswordException;
import com.core.app.auth.repository.UserRepository;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestBody;

public interface UserService extends GenericService<UserEntity> {

  RestResponseDto registerUser(UserSignUpRequestDto signUpRequestDto);

  RestResponseDto confirmRegistration(String requestToken);

  RestResponseDto validateCaptcha(HttpServletRequest request);

  RestResponseDto getCaptcha(HttpServletRequest request);

  RestResponseDto resetPassword(UserResetPasswordDto resetPasswordDto)
      throws WrongCurrentPasswordException;

  void resetPassword(UserEntity user, String newPassword);

  RestResponseDto restorePassword(UserRestorePasswordInitDto request);

  boolean restorePassword(UserEntity user, String newPassword);

  String getCurrentUserEmail();

  RestResponseDto restorePasswordConfirm(@RequestBody UserRestorePasswordDto request);

  UserEntity signUp(String email, String password);

  UserRepository getRepository();

  Optional<UserEntity> getLoginedUser();

  Optional<UserEntity> getUserByEmail(String email);

  UserEntity changeEmail(String email);

  VerificationTokenEntity getVerificationToken(UserEntity user);

  void createVerificationToken(UserEntity user, String token);
}
