package com.core.app.auth.domain.dto;

import lombok.Data;

@Data
public class UserResetPasswordDto {

  private String oldPassword;
  private String newPassword;

}
