package com.core.app.auth.service.impl;

import com.core.app.auth.domain.entity.UserEntity;
import com.core.app.auth.domain.entity.VerificationTokenEntity;
import com.core.app.auth.service.api.VerificationTokenService;
import com.core.app.common.service.impl.GenericServiceImpl;
import com.core.app.auth.repository.VerificationTokenRepository;

import java.security.InvalidParameterException;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class VerificationTokenServiceImpl extends
        GenericServiceImpl<VerificationTokenEntity> implements VerificationTokenService {

  @Autowired
  private VerificationTokenRepository verificationTokenRepository;

  @Override public VerificationTokenEntity generate(UserEntity user) {
    VerificationTokenEntity token = new VerificationTokenEntity(user, UUID.randomUUID()
        .toString());
    verificationTokenRepository.save(token);
    return token;
  }

  @Override public boolean verify(String token) {
    VerificationTokenEntity vtoken = verificationTokenRepository.findByToken(token)
        .orElseThrow(() -> new InvalidParameterException("no such token"));
    return !vtoken.isExpired();
  }

  @Override public VerificationTokenRepository getRepository() {
    return this.verificationTokenRepository;
  }

}
