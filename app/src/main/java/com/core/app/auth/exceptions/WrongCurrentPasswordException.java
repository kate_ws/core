package com.core.app.auth.exceptions;

public class WrongCurrentPasswordException extends Exception {

  public WrongCurrentPasswordException(String message) {
    super(message);
  }

}
