package com.core.app.auth.domain.additional;

public enum RoleNames {
  USER, ADMIN, DEVELOPER
}