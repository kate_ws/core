package com.core.app.auth.repository;

import com.core.app.auth.domain.additional.RoleNames;
import com.core.app.auth.domain.entity.UserEntity;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

  Optional<UserEntity> findByEmail(String email);

  List<UserEntity> findAllByRole(RoleNames roleName);
}
