package com.core.app.auth.controller;


import static com.core.app.common.Constants.HEADER_SECURITY_TOKEN;

import com.core.app.auth.token.SecuredRoles;
import com.core.app.domain.dto.response.RestResponseDto;
import com.core.app.auth.service.api.UserLocaleSettingsService;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "settings")
public class UserLocaleSettingsController {

  @Autowired
  private UserLocaleSettingsService userLocaleSettingsService;

  @Secured({SecuredRoles.USER, SecuredRoles.ADMIN, SecuredRoles.DEVELOPER})
  @ApiImplicitParam(name = HEADER_SECURITY_TOKEN, value = "Header security token",
      required = true, dataType = "string", paramType = "header")
  @GetMapping(value = "user-locale")
  public ResponseEntity<RestResponseDto> getCommonSettings() {
    RestResponseDto response = userLocaleSettingsService.getLogginedUserLocaleSettings();
    return new ResponseEntity<>(response, HttpStatus.OK);

  }

  @Secured({SecuredRoles.USER, SecuredRoles.ADMIN, SecuredRoles.DEVELOPER})
  @ApiImplicitParam(name = HEADER_SECURITY_TOKEN, value = "Header security token",
      required = true, dataType = "string", paramType = "header")
  @PostMapping(value = "user-locale")
  public ResponseEntity<RestResponseDto> setCommonSettings(
      @RequestParam String locale) {
    RestResponseDto response = userLocaleSettingsService.setLogginedUserLocaleSettings(locale);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
