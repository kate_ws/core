package com.core.app.auth.domain.dto;

import lombok.Data;

@Data
public class UserSignUpRequestDto {

  private String email;
  private String password;

}
