package com.core.app.auth.domain.dto;

import lombok.Data;

@Data
public class UserRestorePasswordInitDto {

  private String email;

}
