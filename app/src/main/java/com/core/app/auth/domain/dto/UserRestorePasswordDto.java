package com.core.app.auth.domain.dto;

import lombok.Data;

@Data
public class UserRestorePasswordDto {

  private String newPassword;
  private String token;

}
