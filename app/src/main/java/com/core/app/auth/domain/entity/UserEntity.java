package com.core.app.auth.domain.entity;

import com.core.app.auth.domain.additional.RoleNames;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "users")
public class UserEntity implements Serializable {

  @Id
  @JsonIgnore
  @Column(name = "id")
  @GeneratedValue
  private Long id;

  @Column(name = "email")
  private String email;

  @Column(name = "password")
  @JsonIgnore
  private String password;

  @Column(name = "role")
  @Enumerated(EnumType.STRING)
  private RoleNames role;

  @Column(name = "enabled")
  private boolean enabled = false;

  @Column(name = "locale")
  private String locale;

}