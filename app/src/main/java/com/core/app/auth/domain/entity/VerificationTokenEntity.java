package com.core.app.auth.domain.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "verification_tokens")
public class VerificationTokenEntity implements Serializable {

  private static final int EXPIRATION = 60 * 24 * 3;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private Long id;

  @Column(name = "token")
  private String token;

  @OneToOne(targetEntity = UserEntity.class, fetch = FetchType.EAGER)
  @JoinColumn(nullable = false, name = "user_id")
  private UserEntity user;

  @Column(name = "expiry_date")
  private Date expiryDate;

  public VerificationTokenEntity(UserEntity user, String token) {
    super();
    this.user = user;
    this.token = token;
    this.expiryDate = this.calculateExpiryDate(EXPIRATION);
  }

  public String getToken() {
    return isExpired() ? null : token;
  }

  public boolean isExpired() {
    return new Date().after(this.getExpiryDate());
  }

  private Date calculateExpiryDate(int expiryTimeInMinutes) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Timestamp(calendar.getTime()
        .getTime()));
    calendar.add(Calendar.MINUTE, expiryTimeInMinutes);
    return new Date(calendar.getTime()
        .getTime());
  }

}
