package com.core.app.auth.service.api;

import com.core.app.common.service.api.GenericService;
import com.core.app.domain.dto.response.RestResponseDto;
import com.core.app.auth.domain.entity.UserEntity;

public interface UserLocaleSettingsService extends GenericService<UserEntity> {

  RestResponseDto getLogginedUserLocaleSettings();

  RestResponseDto setLogginedUserLocaleSettings(String locale);

}
