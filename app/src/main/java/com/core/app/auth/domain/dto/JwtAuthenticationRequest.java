package com.core.app.auth.domain.dto;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class JwtAuthenticationRequest {

  @NotNull(message = "Email cannot be null")
  private String email;

  @NotNull(message = "Password cannot be null")
  private String password;

}
