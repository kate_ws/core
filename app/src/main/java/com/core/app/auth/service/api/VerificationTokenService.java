package com.core.app.auth.service.api;

import com.core.app.auth.domain.entity.UserEntity;
import com.core.app.auth.domain.entity.VerificationTokenEntity;
import com.core.app.common.service.api.GenericService;
import com.core.app.auth.repository.VerificationTokenRepository;

public interface VerificationTokenService extends GenericService<VerificationTokenEntity> {

  VerificationTokenEntity generate(UserEntity user);

  boolean verify(String token);

  VerificationTokenRepository getRepository();
}
