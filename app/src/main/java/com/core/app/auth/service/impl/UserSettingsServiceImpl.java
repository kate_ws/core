package com.core.app.auth.service.impl;

import static com.core.app.common.Constants.USER_NOT_FOUND;

import com.core.app.auth.domain.additional.RoleNames;
import com.core.app.auth.domain.dto.UserResetPasswordDto;
import com.core.app.auth.domain.dto.UserRestorePasswordDto;
import com.core.app.auth.domain.dto.UserRestorePasswordInitDto;
import com.core.app.auth.domain.dto.UserSignUpRequestDto;
import com.core.app.auth.domain.entity.UserEntity;
import com.core.app.auth.domain.entity.VerificationTokenEntity;
import com.core.app.auth.event.UserRegistrationSuccessEvent;
import com.core.app.auth.service.api.UserLocaleSettingsService;
import com.core.app.auth.service.api.UserService;
import com.core.app.auth.service.api.VerificationTokenService;
import com.core.app.common.exceptions.BadRequestException;
import com.core.app.common.exceptions.NotFoundException;
import com.core.app.common.service.impl.GenericServiceImpl;
import com.core.app.domain.dto.response.RestResponseDto;
import com.core.app.auth.exceptions.WrongCurrentPasswordException;
import com.core.app.auth.repository.UserRepository;
import com.core.app.auth.repository.VerificationTokenRepository;
import com.core.app.utils.SecurityUtils;
import com.core.app.utils.captcha.CaptchaManager;
import java.nio.charset.StandardCharsets;
import java.security.InvalidParameterException;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Service
public class UserSettingsServiceImpl extends GenericServiceImpl<UserEntity> implements UserService,
        UserLocaleSettingsService {

  @Autowired
  protected ApplicationEventPublisher applicationEventPublisher;
  @Autowired
  protected VerificationTokenRepository verificationTokenRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private PasswordEncoder passwordEncoder;
  @Autowired
  private VerificationTokenService verificationTokenService;
  @Autowired
  private AmqpTemplate amqpTemplate;

  @Override public RestResponseDto registerUser(UserSignUpRequestDto signUpRequestDto) {
    UserEntity signedUpUser = signUp(signUpRequestDto.getEmail(),
        signUpRequestDto.getPassword());

    RestResponseDto response = new RestResponseDto();
    response.setSuccess(true);
    response.setTimestamp();
    response.setData(signedUpUser);
    return response;
  }

  @Override public RestResponseDto confirmRegistration(@RequestParam("token") String requestToken) {
    RestResponseDto response = new RestResponseDto();
    response.setSuccess(false);
    if (verificationTokenService.verify(requestToken)) {
      VerificationTokenEntity token = verificationTokenService.getRepository()
          .findByToken(requestToken)
          .orElseThrow(
              () -> new InvalidParameterException("token not found"));

      UserEntity user = token.getUser();
      if (!user.isEnabled()) {
        user.setEnabled(true);
        userRepository
            .save(user);
        verificationTokenService.getRepository()
            .delete(token);
        response.setSuccess(true);
      } else {
        response.setMessage("This account already confirmed");
      }

    } else {
      response.setMessage("Invalid token");
    }
    response.setTimestamp();
    return response;
  }

  @Override public RestResponseDto validateCaptcha(HttpServletRequest request) {
    RestResponseDto restResponse = new RestResponseDto();
    String captcha = request.getParameter("captcha");
    restResponse.setSuccess(CaptchaManager.validateCaptcha(captcha, request));
    restResponse.setTimestamp();
    if (!restResponse.getSuccess()) {
      restResponse.setMessage("invalid captcha");
      restResponse.setData(CaptchaManager.generateCaptcha(request));
    }
    return restResponse;
  }

  @Override public RestResponseDto getCaptcha(HttpServletRequest request) {
    RestResponseDto restResponse = new RestResponseDto();
    restResponse.setSuccess(true);
    restResponse.setData(CaptchaManager.generateCaptcha(request));
    restResponse.setTimestamp();
    return restResponse;
  }

  @Override
  public RestResponseDto resetPassword(@RequestBody UserResetPasswordDto resetPasswordDto)
      throws WrongCurrentPasswordException {
    RestResponseDto response = new RestResponseDto();
    String oldPassword = resetPasswordDto.getOldPassword();
    String newPassword = resetPasswordDto.getNewPassword();
    String username = SecurityContextHolder.getContext()
        .getAuthentication()
        .getName();

    UserEntity user = userRepository
        .findByEmail(username)
        .orElseThrow(() -> new UsernameNotFoundException(
            String.format("No user found with email '%s'.", username)));
    boolean result = this.resetPassword(user, oldPassword, newPassword);
    response.setSuccess(result);
    if (!result) {
      throw new WrongCurrentPasswordException("Old password is wrong!");
    }
    response.setTimestamp();
    return response;
  }

  @Override public void resetPassword(UserEntity user, String newPassword) {
    user.setPassword(passwordEncoder.encode(newPassword));
    userRepository.save(user);
  }

  private boolean resetPassword(UserEntity user, String oldPassword, String newPassword) {
    if (newPassword.isEmpty() || oldPassword.isEmpty()) {
      return false;
    }
    if (user != null && passwordEncoder.matches(oldPassword, user.getPassword())) {
      resetPassword(user, newPassword);
      return true;
    }
    return false;
  }

  @Override public RestResponseDto restorePassword(UserRestorePasswordInitDto request) {
    RestResponseDto response = new RestResponseDto();
    UserEntity user = userRepository
        .findByEmail(request.getEmail())
        .orElseThrow(
            () -> new UsernameNotFoundException(String.format("No user found with such email")));
    VerificationTokenEntity token = verificationTokenService.generate(user);
    MessageProperties props = new MessageProperties();
    props.setHeader("token", token.getToken());
    props.setHeader("recipient", user.getEmail());
    amqpTemplate.convertAndSend("restore_password",
        new Message("".getBytes(StandardCharsets.UTF_8), props));
    return response;
  }


  @Override public boolean restorePassword(UserEntity user, String newPassword) {
    if (newPassword.isEmpty()) {
      return false;
    }
    resetPassword(user, newPassword);
    return true;
  }

  @Override
  public RestResponseDto restorePasswordConfirm(@RequestBody UserRestorePasswordDto request) {
    RestResponseDto response = new RestResponseDto();
    response.setSuccess(false);
    if (verificationTokenService.verify(request.getToken())) {
      UserEntity user = verificationTokenService.getRepository()
          .findByToken(request.getToken())
          .orElseThrow(
              () -> new UsernameNotFoundException("there are no user related to this token"))
          .getUser();
      response.setSuccess(true);
      this.restorePassword(user, request.getNewPassword());

    } else {
      response.setMessage("invalid token");
    }
    response.setTimestamp();
    return response;
  }

  @Override public String getCurrentUserEmail() {
    Authentication authentication = SecurityContextHolder.getContext()
        .getAuthentication();
    if (authentication.isAuthenticated()) {
      return authentication.getName();
    }
    return null;
  }

  @Override public UserEntity signUp(String email, String password) {
    UserEntity newUser = new UserEntity();
    try {
      newUser.setEmail(email);
      newUser.setPassword(passwordEncoder.encode(password));
      newUser.setRole(RoleNames.USER);

      userRepository.save(newUser);
      UserRegistrationSuccessEvent event = new UserRegistrationSuccessEvent(this);
      event.setUser(newUser);
      event.setRecipient(email);
      applicationEventPublisher.publishEvent(event);

    } catch (Exception e) {
      throw new RuntimeException("this email already used by another account");
    }
    return newUser;
  }

  @Override public UserRepository getRepository() {
    return this.userRepository;
  }

  @Override public Optional<UserEntity> getLoginedUser() {
    String email = SecurityUtils.getCurrentUserEmail();
    return userRepository.findByEmail(email);
  }

  @Override public Optional<UserEntity> getUserByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  @Override public UserEntity changeEmail(String email) {
    String uemail = SecurityUtils.getCurrentUserEmail();
    if (Objects.isNull(uemail)) {
      throw new RuntimeException("User is not authorized");
    }
    if (!uemail.equals(email)) {
      UserEntity unchangedUser = userRepository.findByEmail(uemail)
          .orElseThrow(() -> new NotFoundException(USER_NOT_FOUND));
      UserEntity changeUser = new UserEntity();
      changeUser.setEmail(email);
      changeUser.setPassword(unchangedUser.getPassword());
      changeUser.setRole(unchangedUser.getRole());
      changeUser.setEnabled(unchangedUser.isEnabled());
      changeUser.setLocale(unchangedUser.getLocale());

      userRepository.save(changeUser);
      UserRegistrationSuccessEvent event = new UserRegistrationSuccessEvent(this);
      event.setUser(changeUser);
      event.setRecipient(email);
      applicationEventPublisher.publishEvent(event);
      userRepository.delete(unchangedUser);

      return changeUser;
    } else {
      throw new BadRequestException("emails are equal");
    }
  }

  @Override public VerificationTokenEntity getVerificationToken(UserEntity user) {
    return verificationTokenRepository.findByUser(user);
  }

  @Override public void createVerificationToken(UserEntity user, String token) {
    VerificationTokenEntity verificationToken = new VerificationTokenEntity(user, token);
    this.verificationTokenRepository.save(verificationToken);
  }

  @Override public RestResponseDto getLogginedUserLocaleSettings() {
    Optional<UserEntity> loggined = getLoginedUser();
    UserEntity usrLoggined = loggined.isPresent() ? loggined.get() : new UserEntity();
    return setLocaleRestResponseDto(usrLoggined.getLocale(), "locale not setLogginedUserLocaleSettings");
  }

  @Override public RestResponseDto setLogginedUserLocaleSettings(String locale) {
    Optional<UserEntity> loggined = getLoginedUser();
    UserEntity usrLoggined = loggined.isPresent() ? loggined.get() : new UserEntity();
    usrLoggined.setLocale(locale);
    userRepository.save(usrLoggined);
    return setLocaleRestResponseDto(usrLoggined.getLocale(), "locale not setLogginedUserLocaleSettings");
  }

  private RestResponseDto setLocaleRestResponseDto(String responseResult, String message) {

    RestResponseDto response = new RestResponseDto();
    if (Objects.isNull(responseResult)) {
      response.setData(new String());
      response.setMessage(message);
    } else {
      response.setData(responseResult);
    }
    return response;
  }

}
