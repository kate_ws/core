package com.core.app.auth.repository;

import com.core.app.auth.domain.entity.CommonSettingsEntity;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommonSettingsRepository extends JpaRepository<CommonSettingsEntity, Long> {

  Optional<CommonSettingsEntity> findByUserId(Long userId);
}
