package com.core.app.auth.token;

import com.core.app.auth.domain.entity.UserEntity;
import com.core.app.auth.domain.additional.RoleNames;
import com.core.app.auth.domain.dto.JwtUser;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public final class JwtUserFactory {

  private JwtUserFactory() {
  }

  public static JwtUser create(UserEntity user) {
    return new JwtUser(
        user.getId(),
        user.getEmail(),
        user.getPassword(),
        mapToGrantedAuthorities(user.getRole())
    );
  }

  private static List<GrantedAuthority> mapToGrantedAuthorities(RoleNames roleNames) {
    List<RoleNames> authorities = new ArrayList<>();
    authorities.add(roleNames);
    return authorities.stream()
        .map(authority -> new SimpleGrantedAuthority("ROLE_" + roleNames.name()))
        .collect(Collectors.toList());
  }
}
