package com.core.app.auth.service.impl;

import com.core.app.auth.domain.dto.JwtAuthenticationRequest;
import com.core.app.auth.domain.dto.JwtAuthenticationResponse;
import com.core.app.auth.domain.entity.UserEntity;
import com.core.app.auth.token.JwtTokenUtil;
import com.core.app.common.exceptions.AuthenticationException;
import com.core.app.common.exceptions.BadRequestException;
import com.core.app.domain.dto.response.RestResponseDto;
import com.core.app.auth.repository.UserRepository;
import com.core.app.auth.service.api.AuthenticationService;

import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

  @Value("${jwt.header}")
  private String tokenHeader;

  @Autowired
  private JwtTokenUtil jwtTokenUtil;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  @Qualifier("jwtUserService")
  private UserDetailsService userDetailsService;

  @Autowired
  private AuthenticationManager authenticationManager;

  @Override
  public RestResponseDto createToken(@RequestBody JwtAuthenticationRequest authenticationRequest) {
    if (userDetailsService.loadUserByUsername(authenticationRequest.getEmail())
        .isAccountNonLocked()) {
      authenticate(authenticationRequest.getEmail(), authenticationRequest.getPassword());
    }

    final UserDetails userDetails = userDetailsService.loadUserByUsername(
        authenticationRequest.getEmail());
    final String token = jwtTokenUtil.generateToken(userDetails);

    RestResponseDto response = new RestResponseDto();
    response.setSuccess(true);
    response.setTimestamp();
    response.setData(new JwtAuthenticationResponse(token));
    return response;
  }


  @Override
  public RestResponseDto refreshToken(HttpServletRequest request) {
    String authToken = request.getHeader(tokenHeader);
    final String token = authToken.substring(7);

    RestResponseDto response = new RestResponseDto();
    response.setTimestamp();
    if (jwtTokenUtil.canTokenBeRefreshed(token)) {
      response.setSuccess(true);
      response.setData(new JwtAuthenticationResponse(token));

      return response;
    } else {
      response.setSuccess(false);
      response.setMessage("Token can't be refreshed");

      return response;
    }
  }

  private void authenticate(String username, String password) {
    if (Objects.isNull(username) || Objects.isNull(password)) {
      throw new BadRequestException("Username and password cannot be null");
    }

    UserEntity user = userRepository.findByEmail(username)
        .orElseThrow(() -> new AuthenticationException("User not found"));

    if (!user.isEnabled()) {
      throw new AuthenticationException("User is not enabled!");
    }

    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
  }
}
