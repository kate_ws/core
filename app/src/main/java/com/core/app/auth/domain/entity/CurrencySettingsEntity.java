package com.core.app.auth.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import lombok.Data;
import javax.persistence.Id;

@Data
@Entity
@Table(name = "currency_settings")
public class CurrencySettingsEntity implements Serializable {

  @Id
  @JsonIgnore
  @Column(name = "id")
  @GeneratedValue
  private Long id;

  @Column(name = "currency_code")
  private String currencyCode;

  @Column(name = "currency_name")
  private String currencyName;


}
