package com.core.app.auth.listener;

import com.core.app.auth.domain.entity.UserEntity;
import com.core.app.auth.event.UserRegistrationSuccessEvent;
import com.core.app.config.ServerConfig;
import com.core.app.auth.service.impl.UserSettingsServiceImpl;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;


import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

@Component
public class NewUserRegisteredListener implements
    ApplicationListener<UserRegistrationSuccessEvent> {

  @Autowired
  private UserSettingsServiceImpl userService;
  @Autowired
  protected AmqpTemplate amqpTemplate;
  @Autowired
  protected ServerConfig serverConfig;

  private static final String CONFIRMATION_URL = "/sign-in/confirm";

  @Override
  public void onApplicationEvent(UserRegistrationSuccessEvent event) {
    String confirmationLink = this.prepareConfirmationLink(event);
    MessageProperties props = new MessageProperties();
    props.setHeader("confirmation_link", confirmationLink);
    props.setHeader("recipient", event.getRecipient());
    Message message = new Message("".getBytes(StandardCharsets.UTF_8), props);
    amqpTemplate.convertAndSend("registration_confirmation", message);
  }

  private String prepareConfirmationLink(UserRegistrationSuccessEvent event) {
    UserEntity user = event.getUser();
    String token = UUID.randomUUID()
        .toString();
    userService.createVerificationToken(user, token);
    return serverConfig.getHost() + CONFIRMATION_URL + "?token=" + token;
  }
}
