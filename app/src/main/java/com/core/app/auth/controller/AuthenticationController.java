package com.core.app.auth.controller;

import static com.core.app.common.Constants.HEADER_SECURITY_TOKEN;

import com.core.app.auth.domain.dto.JwtAuthenticationRequest;
import com.core.app.auth.service.api.AuthenticationService;
import com.core.app.auth.token.SecuredRoles;
import com.core.app.common.exceptions.AuthenticationException;
import com.core.app.domain.dto.response.RestResponseDto;
import io.swagger.annotations.ApiImplicitParam;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController {

  @Autowired
  private AuthenticationService authenticationService;

  @PostMapping(value = "${jwt.route.authentication.path}")
  public ResponseEntity<RestResponseDto> createAuthenticationToken(
      @RequestBody @Valid JwtAuthenticationRequest authenticationRequest)
      throws AuthenticationException {
    RestResponseDto response = authenticationService.createToken(authenticationRequest);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  @ApiImplicitParam(name = HEADER_SECURITY_TOKEN, value = "Header security token",
      required = true, dataType = "string", paramType = "header")
  @Secured({SecuredRoles.USER, SecuredRoles.ADMIN, SecuredRoles.DEVELOPER})
  @GetMapping(value = "${jwt.route.authentication.refresh}")
  public ResponseEntity<RestResponseDto> refreshAndGetAuthenticationToken(
      HttpServletRequest request) {
    RestResponseDto response = authenticationService.refreshToken(request);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
