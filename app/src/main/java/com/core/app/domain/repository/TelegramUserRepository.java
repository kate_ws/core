package com.core.app.domain.repository;

import com.core.app.domain.entity.TelegramUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TelegramUserRepository extends JpaRepository<TelegramUserEntity, Long> {

}
