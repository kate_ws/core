package com.core.app.domain.dto.response;

import com.core.app.helpers.DateTimeFormatter;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
public class RestResponseDto {

  private Boolean success;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Object data;

  private Long timestamp;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String message;

  public RestResponseDto() {
    setTimestamp();
    success = true;
  }

  public void setTimestamp() {
    this.timestamp = DateTimeFormatter.getCurrentTimestamp();
  }

  public void setFailure(String message) {
    setSuccess(false);
    setTimestamp();
    setMessage(message);
  }

}
