package com.core.app.domain.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "telegram_user")
public class GroupsEntity implements Serializable {

  @Id
  @Column(name = "id")
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "is_private")
  private boolean isPrivate = false;

  @Column(name = "password")
  private String password;

}