package com.core.app.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class GroupRegisterRequest {

  private Long id;
  private String name;
  private Boolean isPrivate;

  @JsonIgnore
  private String password;

}
