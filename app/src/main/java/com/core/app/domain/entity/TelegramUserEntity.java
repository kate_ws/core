package com.core.app.domain.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "telegram_user")
public class TelegramUserEntity implements Serializable {

  @Id
  @Column(name = "id")
  private Long id;

  @Column(name = "username")
  private String username;

  @Column(name = "firstname")
  private String firstname;

  @Column(name = "secondname")
  private String secondname;

  @Column(name = "bio")
  private String bio;

}