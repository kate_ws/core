package com.core.app.controller;

import static com.core.app.auth.token.SecuredRoles.ADMIN;
import static com.core.app.auth.token.SecuredRoles.DEVELOPER;
import static com.core.app.auth.token.SecuredRoles.USER;
import static com.core.app.common.Constants.HEADER_SECURITY_TOKEN;

import com.core.app.domain.dto.response.RestResponseDto;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("groups/")
@Secured({USER, ADMIN, DEVELOPER})
public class GroupsController {

  @ApiImplicitParam(name = HEADER_SECURITY_TOKEN, value = "Header security token",
      required = true, dataType = "string", paramType = "header")
  @RequestMapping(value = "add", method = RequestMethod.GET)
  public RestResponseDto add(@ModelAttribute String request) {

    return null;
  }


}
