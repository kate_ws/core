package com.core.app.telegram.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
@Slf4j
public abstract class DeleteMassageComponent extends TelegramLongPollingBot {

  public void execute(String chatId, Integer id) {
    try {
      execute(new DeleteMessage(chatId, id));
    } catch (TelegramApiException e) {
      log.error("Failed to delete message \"{}\" due to error: {}", chatId,
          e.getMessage());
    }
  }

}
