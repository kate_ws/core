package com.core.app.telegram.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
@Slf4j
public abstract class MassageSenderComponent extends TelegramLongPollingBot {

  public void execute(String chatId, String text) {
    SendMessage response = new SendMessage();
    response.setChatId(chatId);
    response.setText(text);
    try {
      execute(response);
      log.info("Sent message \"{}\" to {}", text, chatId);
    } catch (TelegramApiException e) {
      log.error("Failed to send message \"{}\" to {} due to error: {}", text, chatId,
          e.getMessage());
    }
  }

}
