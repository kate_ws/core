package com.core.app.telegram;

import com.core.app.telegram.component.DeleteMassageComponent;
import com.core.app.telegram.component.MassageSenderComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

@Component
public class TopicsListner extends TelegramLongPollingBot {

  @Autowired
  private DeleteMassageComponent deleteMassageComponent;

  @Autowired
  private MassageSenderComponent massageSenderComponent;

  private static final Logger logger = LoggerFactory.getLogger(TopicsListner.class);

  @Value("${tg.bot.token}")
  private String token;

  @Value("${tg.bot.username}")
  private String username;

  @Override
  public String getBotToken() {
    return token;
  }

  @Override
  public String getBotUsername() {
    return username;
  }

  @Override
  public void onUpdateReceived(Update update) {
    if (update.hasMessage()) {
      Message message = update.getMessage();
      Long chatId = message.getChatId();
      String userName = message.getAuthorSignature();
      String text = message.getText();
      User username = message.getFrom();
      Integer id = message.getReplyToMessage()
          .getMessageId();
    }
  }
}
