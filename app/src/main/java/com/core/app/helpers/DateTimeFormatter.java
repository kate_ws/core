package com.core.app.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeFormatter {

  private static final String DATE_FORMAT = "MM/dd/yyyy KK:mm:ss";

  public static String formatMilliseconds(long milliseconds, String timezone) {
    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
    sdf.setTimeZone(TimeZone.getTimeZone(timezone));

    return sdf.format(new Date(milliseconds));
  }

  public static String formatMilliseconds(long milliseconds) {

    return formatMilliseconds(milliseconds, "UTC");
  }

  public static Long getCurrentTimestamp() {
    Date currentDate = new Date();
    int offset = TimeZone.getTimeZone("UTC")
        .getOffset(currentDate.getTime());

    return (currentDate.getTime() + offset) / 1000;
  }
}
