package com.core.app.utils;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {

  public static String getCurrentUserEmail() {
    Authentication auth = SecurityContextHolder.getContext()
        .getAuthentication();
    if (!(auth instanceof AnonymousAuthenticationToken)) {
      return SecurityContextHolder.getContext()
          .getAuthentication()
          .getName();
    } else {
      return null;
    }
  }
}
