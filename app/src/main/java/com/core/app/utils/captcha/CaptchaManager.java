package com.core.app.utils.captcha;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import org.apache.commons.codec.binary.Base64;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.Random;

public class CaptchaManager {

  public static byte[] generateCaptcha(HttpServletRequest request) {
    String captchaString = generateCaptchaText(7);
    try {

      HttpSession session = request.getSession(true);
      session.setAttribute("AUTH_CAPTCHA", captchaString);

      return generateImage(captchaString);

    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public static String generateCaptchaText(int length) {
    String saltChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    StringBuffer captchaStrBuffer = new StringBuffer();
    java.util.Random rnd = new java.util.Random();

    // build a random captchaLength chars salt
    while (captchaStrBuffer.length() < length) {
      int index = (int) (rnd.nextFloat() * saltChars.length());
      captchaStrBuffer.append(saltChars.substring(index, index + 1));
    }
    return captchaStrBuffer.toString();
  }

  public static boolean validateCaptcha(String captcha, HttpServletRequest request) {
    HttpSession session = request.getSession();
    String validCaptcha = session.getAttribute("AUTH_CAPTCHA")
        .toString();
    if (validCaptcha.equals(captcha)) {
      session.removeAttribute("AUTH_CAPTCHA");
      return true;
    }
    return false;
  }

  public static byte[] generateImage(String text) {
    int width = 180;
    int height = 40;
    int whiteNoiseCoeff = 16;
    BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics2D g = image.createGraphics();
    g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
    g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

    g.setColor(Color.white);
    g.fillRect(0, 0, width, height);
    g.setFont(new Font("Serif", Font.ITALIC, 26));
    g.setColor(Color.blue);
    int start = 10;
    byte[] bytes = text.getBytes();

    Random random = new Random();
    for (int i = 0; i < bytes.length; i++) {
      g.setColor(new Color(random.nextInt(255), random.nextInt(255), random.nextInt(255)));
      g.drawString(new String(new byte[]{bytes[i]}), start + (i * 20), (int) (Math.random() * 20 + 20));
    }
    g.setColor(Color.white);
    for (int i = 0; i < whiteNoiseCoeff; i++) {
      g.drawOval((int) (Math.random() * 160), (int) (Math.random() * 10), 30, 30);
    }
    drawLinesOnGraphics(g,1,width);
    g.dispose();
    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    try {
      ImageIO.write(image, "png", bout);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    return bout.toByteArray();
  }

  private static void drawLinesOnGraphics(Graphics2D graphics, int numberOfLines, int xpos) {
    Random random = new Random();
    for (int i = 0; i < numberOfLines; i++) {
      int ypos = random.nextInt(40);
      graphics.setColor(new Color(random.nextInt(255), random.nextInt(255), random.nextInt(255)));
      graphics.drawLine(0, ypos, xpos, ypos);
    }
  }
}
