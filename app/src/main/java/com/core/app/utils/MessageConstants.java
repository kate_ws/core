package com.core.app.utils;

/**
 * Util class to map int variables to appropriate message. Used in conjunction with {@link
 * org.springframework.context.MessageSource}.
 */
public class MessageConstants {

  public static final String WELCOME = "WELCOME";

}
