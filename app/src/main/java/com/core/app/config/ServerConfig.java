package com.core.app.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServerConfig {

  @Value("${server.port}")
  private String port;

  @Value("${server.host}")
  private String host;

  public String getPort() {
    return port;
  }

  public String getHost() {
    return host;
  }

}
