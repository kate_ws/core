package com.core.app.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class PropertiesConfig {

  @Bean
  public MessageSource messageSource() {
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBeanClassLoader(PropertiesConfig.class.getClassLoader());
    messageSource.setBasenames("message/messages");
    messageSource.setDefaultEncoding("UTF-8");
    return messageSource;
  }

}
