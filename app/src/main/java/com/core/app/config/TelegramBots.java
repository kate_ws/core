package com.core.app.config;

import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.ApiContextInitializer;

@Configuration
public class TelegramBots {

  static {
    ApiContextInitializer.init();
  }

}
