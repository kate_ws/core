package com.core.app.common.i18n;

import java.util.Locale;

public interface MessageService {

  String getMessage(String messageCode, Object... messageParam);

  String getMessage(String messageCode, Locale locale, Object... messageParam);
}
