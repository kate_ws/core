package com.core.app.common;

public interface Constants {

  String HEADER_SECURITY_TOKEN = "Authorization";

  String USER_NOT_FOUND = "User not found";
  String SETTINGS_NOT_FOUND = "Settings not found";
}
