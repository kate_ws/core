package com.core.app.common.service.api;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface GenericService<T extends Serializable> {

  Optional<T> findById(Long id);

  List<T> list();

  T createOrUpdate(T item);

  Collection<T> saveEntities(Collection<T> entities);

  void delete(Long id);

  void deleteEntities(Collection<T> entities);
}
