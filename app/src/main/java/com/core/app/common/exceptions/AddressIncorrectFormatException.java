package com.core.app.common.exceptions;

public class AddressIncorrectFormatException extends RuntimeException {

  public AddressIncorrectFormatException(String address) {
    super("Address " + address + " not in property format.");
  }
}
