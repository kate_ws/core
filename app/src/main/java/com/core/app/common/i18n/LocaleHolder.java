package com.core.app.common.i18n;

import static org.springframework.web.context.WebApplicationContext.SCOPE_REQUEST;

import java.util.Locale;
import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Data
@Component
@Scope(scopeName = SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class LocaleHolder {

  private Locale locale;

}

