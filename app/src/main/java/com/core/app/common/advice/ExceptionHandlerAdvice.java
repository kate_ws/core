package com.core.app.common.advice;


import com.core.app.common.exceptions.AuthenticationException;
import com.core.app.common.exceptions.BadRequestException;
import com.core.app.common.exceptions.NotFoundException;
import com.core.app.domain.dto.response.RestResponseDto;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice(basePackages = {"com.core.app"},
    annotations = RestController.class)
public class ExceptionHandlerAdvice {

  @ExceptionHandler(BadRequestException.class)
  public ResponseEntity<RestResponseDto> handleBadRequestException(BadRequestException ex) {
    RestResponseDto response = getRestResponseDto(ex.getMessage());
    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler({AuthenticationException.class})
  public ResponseEntity<RestResponseDto> handleAuthenticationException(AuthenticationException e) {
    RestResponseDto response = getRestResponseDto(e.getMessage());
    return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
  }

  @ExceptionHandler({NotFoundException.class})
  public ResponseEntity<RestResponseDto> handleNotFoundException(AuthenticationException e) {
    RestResponseDto response = getRestResponseDto(e.getMessage());
    return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  protected ResponseEntity<RestResponseDto> handleArgNotValidException(
      MethodArgumentNotValidException exception) {

    StringBuilder errorMessage = new StringBuilder();

    try {
      List<ObjectError> errors = exception.getBindingResult()
          .getAllErrors();

      for (ObjectError error : errors) {

        if (error instanceof FieldError) {
          String messageCode = error.getDefaultMessage();
          String messageField = ((FieldError) error).getField();
          errorMessage
              .append("Field ")
              .append(messageField)
              .append(" : ")
              .append(messageCode);
        } else {
          errorMessage.append(error.getObjectName());
        }

        errorMessage.append("; ");
      }
    } catch (Exception e) {
      errorMessage.append("Unable to build error message: ")
          .append(e.getMessage());
    }

    RestResponseDto restResponseDto = getRestResponseDto(errorMessage.toString());
    return new ResponseEntity<>(restResponseDto, HttpStatus.UNPROCESSABLE_ENTITY);
  }

  private RestResponseDto getRestResponseDto(String message) {
    RestResponseDto response = new RestResponseDto();
    response.setSuccess(false);
    response.setTimestamp();
    response.setMessage(message);
    return response;
  }
}
