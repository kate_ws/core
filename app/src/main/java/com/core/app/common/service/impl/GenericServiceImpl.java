package com.core.app.common.service.impl;

import com.core.app.common.service.api.GenericService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract class GenericServiceImpl<T extends Serializable> implements GenericService<T> {

  @Override
  public Optional<T> findById(Long id) {
    return getRepository().findById(id);
  }

  @Override
  public List<T> list() {
    return getRepository().findAll();
  }

  @Override
  public T createOrUpdate(T item) {
    return getRepository().save(item);
  }

  @Override
  public Collection<T> saveEntities(Collection<T> entities) {
    return getRepository().saveAll(entities);
  }

  @Override
  public void delete(Long id) {
    getRepository().deleteById(id);
  }

  @Override
  public void deleteEntities(Collection<T> entities) {
    getRepository().deleteAll();
  }

  protected abstract JpaRepository<T, Long> getRepository();
}
