package com.core.app.common.i18n;

import java.util.Locale;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements MessageService {

  @Autowired
  private MessageSource messageSource;

  private Locale defaultLocale;

  @PostConstruct
  public void init() {
    defaultLocale = Locale.US;
  }

  // For testing purposes
  @Override
  public String getMessage(String messageCode, Object... messageParam) {
    return getMessage(messageCode, defaultLocale, messageParam);
  }

  @Override
  public String getMessage(String messageCode, Locale locale, Object... messageParam) {
    return messageSource.getMessage(messageCode, messageParam, locale);
  }

  public MessageSource getMessageSource() {
    return messageSource;
  }

  public void setMessageSource(MessageSource messageSource) {
    this.messageSource = messageSource;
  }
}
